#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# (c) Shrimadhav U K

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import logging
import os

import hypercorn.asyncio

from quart import (
    Quart,
    jsonify,
    request
)

from telethon import TelegramClient
from telethon.sessions import StringSession


# the secret configuration specific things
if bool(os.environ.get("ENV", False)):
    from sample_config import Config
else:
    from config import Config


# the logging things
logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)
LOGGER = logging.getLogger(__name__)


# TODO: is there a better way?
APP_ID = Config.APP_ID
API_HASH = Config.API_HASH
HU_STRING_SESSION = Config.HU_STRING_SESSION
TG_DUMP_CHANNEL_ID = Config.TG_DUMP_CHANNEL_ID
PORT = Config.PORT

HELP_STICKER = """Please enter an 'url' parameter,
to get the Instant View tags, outside your Telegram!

Powered by @CheckRestrictionsBot"""
NO_IV_AVAILABLE = "There does not seem to be an Instant View available for this link..!"

# a dictionary to store the currently running commands
AKTIFPERINAHT = {}

TG_CLIENT = TelegramClient(
    StringSession(HU_STRING_SESSION),
    APP_ID, 
    API_HASH
)

# Quart app
app = Quart(__name__)
app.secret_key = 'CHANGE THIS TO SOMETHING SECRET'



# Start the client before we start serving with Quart
@app.before_serving
async def startup():
    await TG_CLIENT.connect()


# After we're done serving (near shutdown), clean up the client
@app.after_serving
async def cleanup():
    await TG_CLIENT.disconnect()


@app.route("/", methods=["GET", "POST"])
async def root():
    retrn_nosj = {}
    # me_o_jbo = await TG_CLIENT.get_me()
    # LOGGER.info(me_o_jbo.stringify())
    url_query = request.args.get("url")
    if not url_query:
        retrn_nosj["URL"] = url_query
        retrn_nosj["ERR"] = HELP_STICKER
        return jsonify(retrn_nosj)
    # LOGGER.info(query)
    tg_fwded_mesg = await TG_CLIENT.send_message(
        entity=TG_DUMP_CHANNEL_ID,
        message=url_query,
        reply_to=2,
        link_preview=True
    )
    # LOGGER.info(tg_fwded_mesg.stringify())
    #
    if tg_fwded_mesg.media and \
        tg_fwded_mesg.media.webpage and \
        tg_fwded_mesg.media.webpage.cached_page:
        cached_page = tg_fwded_mesg.media.webpage.cached_page
        retrn_nosj["URL"] = cached_page.url
        retrn_nosj["TEXT_CONTENTS"] = [page_block.to_dict() for page_block in cached_page.blocks]

    #
    if len(retrn_nosj) == 0:
        retrn_nosj["URL"] = url_query
        retrn_nosj["ERR"] = NO_IV_AVAILABLE
    return jsonify(retrn_nosj)


async def main():
    from hypercorn.config import Config
    #
    config = Config()
    HOST = "0.0.0.0"
    # https://t.me/MarieOT/22915
    config.bind = [f"{HOST}:{PORT}"]
    # As an example configuration setting
    #
    await hypercorn.asyncio.serve(app, config)


# By default, `Quart.run` uses `asyncio.run()`, which creates a new asyncio
# event loop. If we create the `TelegramClient` before, `telethon` will
# use `asyncio.get_event_loop()`, which is the implicit loop in the main
# thread. These two loops are different, and it won't work.
#
# So, we have to manually pass the same `loop` to both applications to
# make 100% sure it works and to avoid headaches.
#
# To run Quart inside `async def`, we must use `hypercorn.asyncio.serve()`
# directly.
#
# This example creates a global client outside of Quart handlers.
# If you create the client inside the handlers (common case), you
# won't have to worry about any of this, but it's still good to be
# explicit about the event loop.
if __name__ == '__main__':
    TG_CLIENT.loop.run_until_complete(main())
